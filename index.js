/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => 'hello world';

exports.stripPrivateProperties = (privateProper, fullObject) => {
    let copyObj = JSON.parse(JSON.stringify(fullObject));
    copyObj.forEach((throughThis) => privateProper.every(elem => delete throughThis[elem]));
    return copyObj;
};

exports.excludeByProperty = (excludeProp, properties) => {
    return properties.filter(elem => (elem[excludeProp] === undefined));
};

exports.sumDeep = (checkObj) => {
    return checkObj.map(key => ({ [Object.keys(key)]: Object.values(key)[0].map(value => Object.values(value)[0]).reduce((a, b) => a + b, 0) }));
    // todo We can use the following solution if we assume that we exactly know what are the names of variables in the array of objects:
    // return checkObj.map(({ objects }) => ({ objects: objects.map(({ val }) => val).reduce((a, b) => a + b, 0) }))
};

exports.applyStatusColor = (firstArg, secondArg) => {
    let newArr = [];
    secondArg.map(num => Object.values(firstArg).map((color, index) => color.includes(num.status) ? newArr.push({ ...num, color: Object.keys(firstArg)[index] }) : newArr));
    return newArr;
};

exports.createGreeting = (fireOff, greeting) => {
    return name => fireOff(greeting, name);
};

exports.setDefaults = (checkArray) => {
    return datas => datas.hasOwnProperty(Object.keys(checkArray)[0]) ? datas : { ...datas, ...checkArray };
};

exports.fetchUserByNameAndUsersCompany = (giveName, datas) => {
    const resultStatus = datas.fetchStatus();
    const resultUsers = datas.fetchUsers();

    const result = Promise.all([resultStatus, resultUsers]).then((findings) => {
        const middle = {
            status: findings[0],
            user: findings[1].find(({ name }) => name === giveName)
        };
        return datas.fetchCompanyById(middle.user.companyId).then((tore) => ({ company: tore, ...middle }));
    });
    return result;
};
